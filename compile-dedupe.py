#!/usr/bin/env python3
from pathlib import Path
from subprocess import run
from shinjuku.search import dijkstra, read_components

def comp_keyfunc(s):
    """Key function for sorting components by nature of output."""
    res = s.split(">")[-1]
    if not res: return ("s", 0, "", s)
    # Otherwise, it is an apgcode
    prefix = res.split("_")[0][1:]
    return (prefix[0], int(prefix[1:]), res, s)

def write_comps_to_file(fname, comp_list, sort=True):
    """Writes comp_list to the file fname, sorting using comp_keyfunc if asked for.
    fname does not have the .sjk extension; assume fname contains distinct components."""
    if sort: comp_list.sort(key=comp_keyfunc)
    with open(f"{fname}.sjk", 'w') as f:
        for comp in comp_list:
            print(comp, file=f)

# Pull the latest batch of components as part of the compilation
run("curl https://gol.hatsya.co.uk/textsamples/xs0_contrib/b3s23/synthesis | grep '>' > shinjuku/comp/comp-port.sjk", shell=True)
min_paths = dijkstra()

with open("shinjuku/aliases/1s21", 'r') as f:
    raw = f.readlines()
edges = [] # components in min-paths-to-xs21.sjk
tree_objs = set()
for line in raw:
    lexapg, apgcode = line.split()
    if apgcode not in min_paths:
        continue
    curr = apgcode
    while curr != "" and curr not in tree_objs:
        tree_objs.add(curr)
        N, pred, comp_line = min_paths[curr]
        edges.append(comp_line)
        curr = pred
write_comps_to_file("shinjuku/comp/min-paths-to-xs21", edges)

# Read the below files to form the (unorganised component) pool
pool = ["min-paths-to-xs21", "isolated-edges", "comp-pool", "comp-port", "large-still-lifes"]
pool = [f"shinjuku/comp/{fn}.sjk" for fn in pool]
pool = read_components(pool)
# Dedupe the pool against min-paths-to-xs21's edges
pool -= set(edges)
# But also dedupe against the subfolders in comp and large-still-lifes.sjk
pool -= read_components(Path(".").rglob("shinjuku/comp/*/*.sjk"))
pool -= read_components(["shinjuku/comp/large-still-lifes.sjk"])
# Is the component's starting point reachable in the entire min-distance tree?
# If not, put it in isolated-edges.sjk
isolated, rest = [], []
for comp in pool:
    start = comp.split(">")[0].partition("+")[0]
    if start not in min_paths: isolated.append(comp)
    else:                      rest.append(comp)

write_comps_to_file("shinjuku/comp/isolated-edges", isolated)
write_comps_to_file("shinjuku/comp/comp-pool", rest)

# Delete the temporary file
run("rm shinjuku/comp/comp-port.sjk", shell=True)
