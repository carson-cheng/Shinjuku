import lifelib
sess = lifelib.load_rules("b3s23")
# Main, 2-state lifetree that is used for instantiating patterns
lt = sess.lifetree(n_layers=1)
# 4-layer lifetree used by the fragments system
lt4 = sess.lifetree(n_layers=4)
