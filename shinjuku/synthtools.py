import os
from shinjuku import lt
from .gliderset import gset
from .search import dijkstra, lookup_synth
from .transcode import encode_comp, realise_comp
from .checks import rewind_check

# These functions are designed for terminal use. One-letter aliases
# are provided at the end of this file for the most commonly used ones,
# so `from shinjuku import *` provides access to said aliases.

def split_mosaic(mosaic, radius=17, assume_rows=False, maxtime=256):
    """Splits a mosaic of components without respect to order; returns a list of Patterns.
    radius controls how far polyplets must be to be considered part of different components.
    assume_rows splits on rows beforehand, effective for parsing Niemiec RLEs."""
    if isinstance(mosaic, str): # convert RLE to Pattern
        mosaic = mosaic.replace('x', 'b').replace('b =', 'x =')
        mosaic = mosaic.replace('y', 'b').replace('b =', 'y =')
        mosaic = lt.pattern(mosaic)
    env = mosaic + mosaic[2] + mosaic[4] + mosaic[6] + mosaic[8] + mosaic[10] + mosaic[12] + mosaic[14]
    for i in range(16, 64, 4): env += mosaic[i]
    for i in range(64, maxtime, 8): env += mosaic[i]

    if assume_rows:
        comps = env.components(halo='125o$125o$125o$125o$125o!')
        comps = [(c & mosaic) for c in comps]
        print(f"# {len(comps)} rows")
        synthparts = [split_mosaic(c, radius=radius) for c in comps]
        return [x for y in synthparts for x in y]
    diameter = radius * 2 + 1
    halo = lt.pattern(f'{diameter}o$' * diameter).centre()
    comps = env.components(halo=halo)
    comps = [c & mosaic for c in comps]
    comps = [c for c in comps if c.population > 5]
    return comps

def encode_mosaic(rle, radius=17):
    """Wrapper around split_mosaic(), intended for terminal use.
    Prints the possibly unordered encoded components as a string."""
    print("\n".join(encode_comp(c) for c in split_mosaic(rle, radius=radius)))

def parse_niemiec(flist, outf):
    """Parses the files named in flist Niemiec-style, returning a list of components in outf.
    Comment lines mark those patterns that failed to parse properly."""
    with open(outf, 'w') as out:
        for filename in flist:
            print(f"# Reading {filename}...", file=out)
            try:
                with open(filename, 'r') as f: pat = f.read()
                codes = [encode_comp(c) for c in split_mosaic(pat, radius=12, assume_rows=True)]
                for c in codes:
                    if rewind_check(*realise_comp(c, True)):
                        print(c, file=out)
                    else:
                        print(f"# Invalid component: {c}", file=out)
            except FileNotFoundError:
                continue
            except (KeyError, TypeError):
                print("# Incomprehensible pattern!", file=out)

def island_delete(soup, pat, rew):
    """Helper to pred() that deletes islands from soup[birth_t - rew] such that
    pat still persists at soup[birth_t + rew]. If birth_t < rew then return None,
    otherwise also return birth_t as the second argument of a tuple."""
    # Struzik search for the first generation where the given pattern occurs.
    # f(k) below is whether the soup has produced the object at generation k.
    period = pat.period
    def f(k):
        sn = soup[k]
        for p in range(period):
            if sn != sn.replace(pat[p], lt.pattern(), halo="3o$3o$3o", orientations="rotate4reflect"):
                return True
        return False
    for expo in range(64):
        if f(2 ** expo):
            lower, upper = 1, 2 ** expo
            break
    while upper - lower > 1:
        mid = (lower + upper) // 2
        if f(mid): upper = mid
        else: lower = mid
    if upper < rew: return None
    soup = soup[upper - rew]
    # Delete most unnecessary islands
    for c in soup.components(halo="3o$3o$3o"):
        soup -= c
        if not f(2 * rew): soup += c
    soup = soup[1]
    for c in soup.components(halo="3o$3o$3o"):
        soup -= c
        if not f(2 * rew): soup += c
    return (soup.centre(), upper)

def pred(apgcode, sym="C1", rew=50, del_islands=True):
    """"Preds" (finds predecessor patterns for) the given object using soups of the
    specified symmetry from Catagolue. Rewinds rew generations before deleting islands
    if del_islands is True."""
    mosaic = lt.pattern()
    successful = 0
    pat = lt.pattern(apgcode)
    for (n, soup) in enumerate(pat.download_samples()[sym], 1):
        if del_islands:
            try: soup, birth_t = island_delete(soup, pat, rew)
            except TypeError: continue
            print(f"{n} -> {birth_t}")
        mosaic += soup(successful // 10 * 100, successful % 10 * 100)
        successful += 1
    mosaic.save("soups.rle")
    os.remove("tempfile")

p = pred
e = encode_mosaic
r = lambda s: print(realise_comp(s).rle_string())

def l(*pats):
    """Looks up syntheses for one or more patterns. For each pattern provided,
    prints out its cost and an RLE."""
    min_paths = dijkstra()
    for pat in pats:
        N, synth_pat = lookup_synth(min_paths, pat)
        print(f"{pat}: {N} gliders")
        print(synth_pat.rle_string().partition('\n')[2].strip())
        print()
