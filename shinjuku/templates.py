import re
from shinjuku import lt, lt4
from .transforms import oriens, derive_tf
from .transcode import comp_canonical_time, encode_comp, realise_comp
from .gliderset import gset
from .checks import rewind_check
integer_re = re.compile(r"-?[0-9]+")
zoi = lt.pattern("b3o$5o$5o$5o$b3o")(-2, -2)

# The following is an example of a template:
# 084_08csvfucz06773_42+4<9 -1///4 -2@0L4 3
# which turns a tub into a barge.

def template_zones(constell, gliders, extra_affected=lt.pattern()):
    """gliders is at canonical position, and constell transformed
    along with it. From this time minus 4, find
    (a) the number of generations between canonical time and stabilisation, N
    (b) the affected cells (periodic behaviour affected on the way to
    stabilisation)
    (c) the toggleset or difference between canonical and canonical[N],
    intersected with (b).convolve(range-2 zone of influence) or (b')
    (d) the constellation's cells in (b') that are on at canonical time
    Returns (a, b, c, d).
    
    extra_affected specifies additional cells that are in the affected zone,
    aligned to constell and gliders."""
    N = -4
    expected = constell[-4]
    affected = gliders[-4].s
    observed = expected + affected
    while not observed.oscar(eventual_oscillator=False, verbose=False): # not stabilised yet
        expected = expected[1]
        observed = observed[1]
        affected += observed ^ expected
        N += 1
    # N now holds the time to stabilisation from canonical
    toggleset = constell ^ observed
    # Run for a few more generations to catch any last deviations from the baseline
    for q in range(max(constell.period, observed.period)):
        expected = expected[1]
        observed = observed[1]
        affected += observed ^ expected
    affected += extra_affected
    affected_zone = affected.convolve(zoi)
    return (N, affected, affected_zone & toggleset, affected_zone & constell)

def encode_template(comp_str_or_rle):
    """Extracts a template from the given component string or B3/S23/LifeHistory RLE,
    returning the encoded template. If the RLE is in LifeHistory, its marked cells are added to the
    affected region."""
    # Convert component string to RLE if necessary
    rle = realise_comp(comp_str_or_rle).rle_string() if ">" in comp_str_or_rle else comp_str_or_rle
    # Extract the live and mark layers
    if "LifeHistory" in rle:
        lyrs = lt4.pattern(rle).layers()
        live, mark = lt.unify(lyrs[0]), lt.unify(lyrs[3])
    else:
        live = lt.pattern(rle)
        mark = lt.pattern()
    # Lock the gliders to canonical position
    gliders, constell = comp_canonical_time(live)
    canonical_glider_data, transform_str = gliders.ct()
    t = int(integer_re.search(transform_str)[0])
    target = gset.reconstruct(canonical_glider_data)[-t] # comp-canon-time glider set at canonical position
    canon_map = derive_tf(gliders.s, target.s, 1)[1][0]
    constell = constell(*canon_map)
    mark = mark(*canon_map)
    # Derive the relevant zones from the patterns at canonical position
    N, affected, toggled, canon_on = template_zones(constell, target, mark)
    zones = lt4.unify(toggled, affected, canon_on)
    zonew = zones.wechsler
    zones_c = lt4.pattern(f"xp0_{zonew}", verify_apgcode=False)
    gliders = target(*derive_tf(zones, zones_c, 1)[1][0])
    res1, res2 = gliders.ct()
    return f"{zonew}+{N}<{res1}{res2}"

def decode_template(temp_str):
    """Decodes temp_str and returns (N, match1, match0, toggleset, gliders),
    where the last four 2-state patterns are aligned."""
    wechsler, glider_data = temp_str.split("<")
    wechsler, N = wechsler.split("+")
    toggleset, affected, match1 = [lt.unify(l) for l in lt4.pattern(f"xp0_{wechsler}", verify_apgcode=False).layers()][:3]
    affected_zone = affected.convolve(zoi)
    match0 = affected_zone - match1
    gliders = gset.reconstruct(glider_data)
    return (int(N), match1, match0, toggleset, gliders)

def apply_template_forward(temp_str, apgcodes, verbose=True):
    """Attempts to apply the template specified by the given string to the
    given apgcodes, such that resulting components run forward from constell.
    Returns a list of components found as strings.
    
    verbose=True prints out components as they are discovered."""
    N, match1, match0, toggleset, gliders = decode_template(temp_str)
    sub_oriens = {o: (match1(o), match0(o), toggleset(o), gliders(o)) for o in oriens}
    if not match1: return [] # can't match empty space; this will happen for "Xerneas" (creating) templates
    
    found_comps = []
    for apgcode in apgcodes:
        local_comps = {}
        base = lt.pattern(apgcode)
        for n in range(base.period):
            for o in oriens:
                match1, match0, toggleset, gliders = sub_oriens[o]
                for (x, y) in base.match(match1, match0).coords():
                    gliders_t = gliders(x, y)
                    # In order for this to be accepted, at the stabilisation time...
                    # (1) The gliders must be infinitely rewindable
                    if not rewind_check(base, gliders_t): continue
                    # (2) After N generations, the whole must have stabilised
                    after_N = (base + gliders_t.s)[N]
                    if not after_N.oscar(eventual_oscillator=False, verbose=False): continue
                    # (3) Within the affected region's zone of influence (sum of match0 and match1),
                    # the toggled set must be exactly as specified after N generations
                    if (after_N ^ base ^ toggleset(x, y)) & (match0 + match1)(x, y): continue
                    # Properly encode the component, then add it to the dictionary...
                    # which then does deduping on output objects
                    minted = encode_comp(base[-4] + gliders_t[-4].s)
                    local_comps[after_N.apgcode] = minted
                    if verbose: print(minted)
        found_comps.extend(list(local_comps.values()))
    return found_comps
