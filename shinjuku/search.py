import os, heapq
from pathlib import Path
from shinjuku import lt
from .transforms import derive_tf, inverse_tf, compose_tf
from .transcode import decode_comp, realise_comp
from .gliderset import gset

curr_folder = os.path.dirname(__file__)
comp_folder = os.path.join(curr_folder, "comp")
# Only the component files in the comp folder beside THIS file!
comp_files = list(Path(curr_folder).rglob("*.sjk"))
name_table = os.path.join(curr_folder, "names")

def read_components(flist=comp_files, verbose=False):
    """Utility function to read all components in the files whose names are
    given in flist. Missing files are silently ignored. Returns a set of components.
    
    The pathnames in flist when it is given as an argument are treated
    relative to the Python interpreter's current location."""
    res = set()
    for fn in flist:
        if verbose: print(f"Reading {fn}")
        try:
            with open(fn, 'r') as f:
                for line in f:
                    line = line.partition("#")[0].rstrip()
                    if line: res.add(line)
        except FileNotFoundError:
            continue
    return res

def dijkstra(seed="", prefer_long=False):
    """Compiles cheapest syntheses from the database from the given seed apgcode
    using a lazy variant of Dijkstra's algorithm. prefer_long=True favours syntheses
    with more steps (which are easier to implement in larger constructions, since fewer
    gliders have to be synchronised per stage) among those with the same number of gliders
    by giving each traversed edge a very small bonus.
    
    To ensure consistency between invocations, if there are multiple same-cost paths leading
    to the same object the path that ends in the lexicographically earliest component line
    is chosen."""
    db = {}
    for line in read_components():
        in_str, n, out_str = decode_comp(line)
        if in_str not in db: db[in_str] = []
        db[in_str].append((n, out_str, line))
    # The format of items in res is
    # {constellation apgcode: [fewest gliders, predecessor apgcode, component string from predecessor]}
    # To allow sorting, each element in the designated heap (priority queue) stores
    # (fewest gliders, constellation apgcode)
    res = {seed: [0, None, None]}
    pq = [(0, seed)]
    while pq:
        dist, curr = heapq.heappop(pq)
        if dist > res[curr][0]: continue
        for neighbour in db.get(curr, []):
            ndist = dist + neighbour[0] - (1e-6 if prefer_long else 0)
            if neighbour[1] not in res or ndist < res[neighbour[1]][0] or \
            ndist == res[neighbour[1]][0] and neighbour[2] < res[neighbour[1]][2]:
                heapq.heappush(pq, (ndist, neighbour[1]))
                res[neighbour[1]] = [ndist, curr, neighbour[2]]
    if prefer_long:
        for k in res: res[k][0] = round(res[k][0])
    return res

def cycle_detect(pat):
    """Given that pat is eventually periodic, returns (p, start),
    where pat begins periodic behaviour of period p at gen start."""
    # We are seeking the smallest n for which f below returns True
    f = lambda n: pat[n].oscar(eventual_oscillator=False, verbose=False, maxexp=20)
    for expo in range(64):
        dct = f(2 ** expo - 1)
        if dct:
            p = dct["period"]
            # Except if f(0) is already True,
            # f(lower) is False and f(upper) is True
            lower, upper = 0, 2 ** expo - 1
            break
    while upper - lower > 1:
        mid = (lower + upper) // 2
        if f(mid): upper = mid
        else: lower = mid
    return (p, upper)

def stitch_lines(lines):
    """Stitches component lines into a full synthesis pattern without locking orientation."""
    res, offset = lt.pattern(), 0
    for line in lines:
        stage = realise_comp(line)
        x, y, width, height = stage.getrect()
        res += stage(offset - x, -y - height // 2)
        offset += width + 30
    return res

def slock(lines, gnf=False):
    """Stitches component lines into a full synthesis pattern, locking orientation and
    aligning the component Patterns to a horizontal line. The name is a shortening
    of Stitch_Lines_Orientation_loCK."""
    frame = ("identity", 0, 0)
    oriented_pats = []
    for line in lines:
        stage = realise_comp(line)
        oriented_pats.append(stage(*inverse_tf(frame)))
        p, start = cycle_detect(stage)
        stage_res = stage[start]
        stage_cform = lt.pattern(stage_res.oscar(return_apgcode=True, eventual_oscillator=False, verbose=False, maxexp=20)['apgcode'], verify_apgcode=False)
        chosen_tf = derive_tf(stage_res, stage_cform, p)[1][0]
        frame = compose_tf(chosen_tf, frame)
    
    # Uniformly transform all components before stitching them together such that
    # if the final object is a spaceship, it travels between east and northeast
    dx, dy = oriented_pats[-1].oscar(verbose=False, maxexp=20)['displacement']
    if dx < 0:
        oriented_pats = [pat("flip_x") for pat in oriented_pats]
        dx *= -1
    if dy > 0:
        oriented_pats = [pat("flip_y") for pat in oriented_pats]
        dy *= -1
    if dx < -dy:
        oriented_pats = [pat("swap_xy_flip") for pat in oriented_pats]

    if gnf:
        # fix phases:
        for i in range(len(oriented_pats) - 1):
            p0 = oriented_pats[i][4096]
            p1 = oriented_pats[i+1]
            g1 = gset.extract(p1)
            p1 = p1 - g1.s
            for k in range(8):
                if p1[-k] == p0:
                    oriented_pats[i+1] = p1[-k] + g1[-k].s
                    break
            else:
                raise RuntimeError('failure')

    res, offset = lt.pattern(), 0

    for pat in oriented_pats:
        if gnf:
            res += pat(offset, 0)
            offset += 160
        else:
            x, y, width, height = pat.getrect()
            res += pat(offset - x, 0)
            offset += width + 30

    return res

def lookup_synth(min_paths, cstr_or_name, orien_lock=True, gnf=False, verbose=False):
    """Using the output of dijkstra(), returns (one of) the cheapest glider syntheses
    for the constellation represented by the given apgcode as a Pattern,
    predeced by the glider count. Names can also be used where they are available.
    orien_lock controls whether orientation/position locking of components is applied."""
    import re
    alnum_re = re.compile(r"[^A-Za-z0-9.]")
    names = {}
    with open(name_table, 'r') as f:
        for l in f:
            apgcode, colon, aliases = l.rstrip().partition(":")
            for alias in aliases.split("|"):
                names[alias] = apgcode
    if cstr_or_name in min_paths:
        cstr = cstr_or_name
    else:
        lower_name = alnum_re.sub("", cstr_or_name).lower()
        if lower_name in names:
            cstr = names[lower_name]
        else:
            return None
    N = min_paths[cstr][0]
    curr, lines = cstr, []
    while curr != "":
        pred = min_paths[curr]
        lines.append(pred[2])
        curr = pred[1]
    if verbose:
        for line in lines[::-1]:
            print(line)
        print()
    if orien_lock:
        res = slock(lines[::-1], gnf=gnf)
    else:
        res = stitch_lines(lines[::-1])
    return (N, res)
