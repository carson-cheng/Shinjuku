from shinjuku import lt
oriens = ("identity", "rot90", "rot180", "rot270", "flip_y", "swap_xy_flip", "flip_x", "swap_xy")
orien_letters = "FLBRflbr"
ocodes = dict(zip(oriens, orien_letters))
inv_ocodes = dict(zip(orien_letters, oriens))
# The action applied first is on top
d8_cayley = ("FLBRflbr",
             "LBRFlbrf",
             "BRFLbrfl",
             "RFLBrflb",
             "frblFRBL",
             "lfrbLFRB",
             "blfrBLFR",
             "rblfRBLF")

def derive_tf(source, target, limit):
    """Given that source and target are two phases of a periodic object
    with period at most limit, return the least k such that target[k] is congruent
    to source, as well as the possible transformations from source to target
    in a form usable with Pattern.__call__(). Most often, target is the canonical
    form of source, derived by lt.pattern(apgcode)."""
    desired = source.wechsler
    for n in range(limit):
        if desired == target[n].wechsler:
            valids = []
            for o in oriens:
                target_r = target[n].getrect()
                source_r = source(o).getrect()
                shift_x = target_r[0] - source_r[0]
                shift_y = target_r[1] - source_r[1]
                if source(o, shift_x, shift_y) == target[n]:
                    valids.append((o, shift_x, shift_y))
            return (n, valids)

def inverse_tf(tf):
    """Inverts the transformation of the Life plane tf in the Pattern.__call__() format."""
    if tf[0] == "rot90": inv_orien = "rot270"
    elif tf[0] == "rot270": inv_orien = "rot90"
    else: inv_orien = tf[0] # All but the above two elements of D_8 are self-inverse
    x, y = lt.pattern("o")(*tf)(inv_orien).coords()[0]
    return (inv_orien, -x, -y)

def compose_tf(tf2, tf1):
    """Composes tf2 to the left of tf1 (i.e. tf1 is applied first, then tf2)."""
    f = orien_letters.index(ocodes[tf1[0]])
    g = orien_letters.index(ocodes[tf2[0]])
    res_orien = inv_ocodes[d8_cayley[g][f]]
    x, y = lt.pattern("o")(*tf1)(*tf2).coords()[0]
    return (res_orien, x, y)
